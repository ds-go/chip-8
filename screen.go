package main

import (
	"log"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
)

type Screen struct {
	Conn   net.Conn
	Input  chan []int
	Render chan []byte
}

func NewScreen(c net.Conn, input chan []int, r chan []byte) *Screen {
	return &Screen{
		Conn:   c,
		Input:  input,
		Render: r,
	}
}

func Websocketexample(rom []byte) {

	tickend := time.Now().UnixNano()
	fpsend := time.Now().UnixNano()
	target := int64((10000000 * 100) / 500)
	targetFps := int64((10000000 * 100) / 60)

	ticker := time.NewTicker(time.Millisecond * 3)
	input := make(chan []int)
	render := make(chan []byte)
	defer ticker.Stop()
	go Start(input, render)
	cpu := NewChip8(rom)
	//power
	for {
		select {
		case i := <-input:
			if len(i) > 1 {
				cpu.key[i[0]] = byte(i[1])
			}
			//boot
			if i[0] == 99 {
				for {
					framestart := time.Now().UnixNano()
					tickStart := framestart - tickend
					fpsStart := framestart - fpsend

					if tickStart > target {
						cpu.execute(cpu.fetchOp())
						if fpsStart > targetFps {

							cpu.tick()
							if cpu.draw {
								render <- cpu.graphics
								cpu.draw = false
							}

							fpsend = time.Now().UnixNano()
						}
						tickend = time.Now().UnixNano()
					}
				}
			}
		}
	}
}

func Start(sys chan []int, render chan []byte) {
	http.ListenAndServe(":6060", http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path == "/render" {
				//serve frontend
				http.ServeFile(w, r, "render.html")
			}
			//handle websockets
			if r.URL.Path == "/echo" {
				conn, _, _, err := ws.UpgradeHTTP(r, w)
				if err != nil {
					log.Println(err)
					return
				}
				go handle(sys, render, conn)
			}
		},
	))
}

func handle(input chan []int, render chan []byte, c net.Conn) {
	defer c.Close()
	player := NewScreen(c, input, render)
	go player.Inputs()
	player.DrawScreen()
}

func (p *Screen) DrawScreen() {
	for {
		select {
		case data := <-p.Render:
			wsutil.WriteServerMessage(p.Conn, 0x02, data)
			//fmt.Println("a", data)
		}
	}
}

//Inputs gets inputs from websocket
func (p *Screen) Inputs() {
	for {
		msg, _, err := wsutil.ReadClientData(p.Conn)
		if err != nil {
			panic(err)
			return
		}
		str := string(msg[:len(msg)])

		if str == "ping" {
			p.Input <- []int{99}
		}

		kv := strings.Split(str, ":")

		if len(kv) > 1 {
			key, _ := strconv.Atoi(kv[0])
			val, _ := strconv.Atoi(kv[1])
			go func() {
				p.Input <- []int{key, val}
			}()
		}

	}
}
