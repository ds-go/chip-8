package main

import (
	"testing"
)

func NewMockCpu() *chip8 {
	GraphicsMemory := make([]byte, 64*32)
	for i := range GraphicsMemory {
		//right to graphics memory
		GraphicsMemory[i] = 0x55
	}
	c := NewChip8([]byte{})
	c.graphics = GraphicsMemory
	return c
}

//Mock Data
//Code, 0xX, 0xY, 0xKK, 0xNNN
func _mockOpcodes() [][]uint16 {
	return [][]uint16{
		[]uint16{0x5230, 0x02, 0x03, 0x30, 0x230},
		[]uint16{0x5460, 0x04, 0x06, 0x60, 0x460},
		[]uint16{0x5220, 0x02, 0x02, 0x20, 0x220},
		[]uint16{0x5FF0, 0x0F, 0x0F, 0xF0, 0xFF0},
		[]uint16{0x5FF3, 0x0F, 0x0F, 0xF3, 0xFF3},
	}
}

func Test_InsClearScreen(t *testing.T) {
	expected := make([]byte, 64*32)
	for i := range expected {
		expected[i] = 0x00
	}
	cpu := NewMockCpu()
	cpu.InsClearScreen(0x00E0)
	for i := range cpu.graphics {
		if expected[i] != 0x00 {
			t.Error("memory not clear")
		}
	}
}

func Test_InsCallSub(t *testing.T) {
	cpu := NewMockCpu()
	cpu.pc = 0
	cpu.sp = 0

	oldstack := cpu.sp
	oldpc := cpu.pc
	cpu.InsCallSub(0x255)

	if cpu.stack[oldstack] != oldpc {
		t.Error("cpu.stack[oldstack] != oldpc")

	}
	if cpu.sp != oldstack+1 {
		t.Error("cpu.sp != oldstack++")
	}

	if cpu.pc != 0x255 {
		t.Error("cpu.pc := 0x255")
	}
}

func Test_InsGotoNNN(t *testing.T) {
	cpu := NewMockCpu()
	cpu.pc = 0
	cpu.InsGotoNNN(0x1222)
	if cpu.pc != 0x222 {
		t.Error("Jump to NNN incorrect expected: got:")
	}
}

func Test_InsRtnSub(t *testing.T) {
	cpu := NewMockCpu()

	expected := uint16(0x03)

	sp := byte(1)
	pc := uint16(1)
	cpu.sp = sp
	cpu.pc = pc
	cpu.stack[0] = expected
	cpu.InsRtnSub(0x00EE)

	if cpu.sp != sp-1 {
		t.Error("cpu.sp != sp-1")
	}
	if cpu.pc != expected+uint16(2) {
		t.Error("cpu.pc != expected pc (+2)")
	}
}

func Test_InsSkipVxNotKK(t *testing.T) {
	//if(Vx!=KK)
	cpu := NewMockCpu()
	cpu.pc = 0
	x := 0x01
	cpu.v[x] = 0x5

	//4XKK
	cpu.InsSkipVxNotKK(0x4122)

	if cpu.pc != 4 {
		t.Error("error Skip v[x] != NN")
	}
}

func Test_InsSkipVxKK(t *testing.T) {
	cpu := NewMockCpu()
	cpu.pc = 0
	cpu.v[0x2] = byte(0x20)
	cpu.InsSkipVxKK(0x3220)
	if cpu.pc != 4 {
		t.Error("InsSkipVxKK")
	}
}

func Test_InsSkipVxVy(t *testing.T) {
	cpu := NewMockCpu()
	cpu.pc = 0
	x := 0x01
	y := 0x02
	cpu.v[x] = 0x5
	cpu.v[y] = 0x5

	cpu.InsSkipVxVy(0x5120)

	if cpu.pc != 4 {
		t.Error("error Skip v[x] != NN")
	}
}

func _testPartial(op uint16, p uint16, t *testing.T) {
	opPartial := op & 0xF000
	if opPartial != p {
		t.Errorf("wrong partial 0x%x", opPartial)
	}
}

func Test_GetAddressX(t *testing.T) {
	data := _mockOpcodes()
	for i := range data {
		actual := GetAddressX(data[i][0])
		expected := data[i][1]
		if actual != expected {
			t.Error("Wrong Adress X returned")
		}
	}
}

func Test_GetAddressY(t *testing.T) {
	data := _mockOpcodes()
	for i := range data {
		actual := GetAddressY(data[i][0])
		expected := data[i][2]
		if actual != expected {
			t.Error("Wrong Adress Y returned")
		}
	}
}

func Test_GetAddressKK(t *testing.T) {
	data := _mockOpcodes()
	for i := range data {
		actual := GetAddressKK(data[i][0])
		expected := data[i][3]
		if actual != expected {
			t.Error("Wrong Adress KK returned")
		}
	}
}

func Test_GetAddressNNN(t *testing.T) {
	data := _mockOpcodes()
	for i := range data {
		actual := GetAddressNNN(data[i][0])
		expected := data[i][4]
		if actual != expected {
			t.Errorf("Wrong Adress NNN returned expected 0x%x actual: 0x%x \n", expected, actual)
		}
	}
}

func Test_InsSetVXKK(t *testing.T) {
	c := NewMockCpu()
	x := 0x01
	kk := 0x22
	c.InsSetVXKK(0x6122)
	if c.v[x] != byte(kk) {
		t.Error("v[x] KK not set")
	}
}

func Test_InsAddKKtoVX(t *testing.T) {
	c := NewMockCpu()

	data := [][]uint16{
		[]uint16{0x01, 1, 0x02, 0x7101},
		[]uint16{0x02, 255, 0x0, 0x7201},
	}

	for i := range data {

		//reset value
		x := data[i][0]

		c.v[x] = byte(data[i][1])

		expected := byte(data[i][2])
		c.InsAddKKtoVX(data[i][3])

		if c.v[x] != expected {
			t.Error("v[x] KK++ incorrect", c.v[x])
		}
	}

}
