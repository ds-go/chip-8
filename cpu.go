package main

import (
	"fmt"
	"math/rand"
)

type chip8 struct {
	memory   []byte   //4096
	pc       uint16   //program counter
	v        []byte   //registers[16]
	i        uint16   //index register
	sp       byte     //stack pointer
	stack    []uint16 //stack
	delay    byte     //delay timer
	sound    int      //sound timer
	romsize  int
	draw     bool
	graphics []byte
	blank    []byte //clear graphics
	key      []byte
}

func NewChip8(data []byte) *chip8 {
	c := &chip8{
		memory:   make([]byte, 4096),
		pc:       0x200,
		v:        make([]byte, 16),
		i:        0,
		sp:       0,
		stack:    make([]uint16, 16),
		delay:    0x00,
		sound:    0,
		romsize:  len(data),
		draw:     true,
		graphics: make([]byte, 64*32),
		key:      make([]byte, 16),
	}

	for i := range c.memory {
		c.memory[i] = 0x00
	}
	for i := range c.graphics {
		c.graphics[i] = 0x00
	}

	if (4096 - 512) < len(data) {
		panic("rom too large")
	}

	//0x200-0xFFF - Program ROM and work RAM
	for i, v := range data {
		c.memory[0x200+i] = v
	}

	//0x050-0x0A0 - Used for the built in 4x5 pixel font set (0-F)
	//each char c.v[x] * 0x05
	font := CreateSysFont()
	for ift := 0; ift < 80; ift++ {
		c.memory[ift] = font[ift]
	}
	return c
}

//tick happens every 60hz
func (c *chip8) tick() {
	if c.delay > 0 {
		c.delay--
	}
	if c.sound > 0 {
		c.sound--
	}
}

func (c *chip8) fetchOp() uint16 {
	x := uint16(c.memory[c.pc])
	y := uint16(c.memory[c.pc+1])
	code := _getOpCode(x, y)
	return code
}

func (c *chip8) execute(op uint16) {
	switch op & 0xF000 {
	case 0x0000:
		switch op & 0x000F {
		//0x00EE: Returns from subroutine
		case 0x0000:
			c.InsClearScreen(op)
			break
		//0x00E0: Clears the screen
		case 0x000E:
			c.InsRtnSub(op)
			break
		}
		break
	//Goto NNN; Jumps to address NNN.
	case 0x1000:
		c.InsGotoNNN(op)
		break
	//Calls subroutine at NNN.
	case 0x2000:
		c.InsCallSub(op)
		break
	//Skip next instruction if Vx = KK.
	case 0x3000:
		c.InsSkipVxKK(op)
		break
	//Skips the next instruction if VX != KK
	case 0x4000:
		c.InsSkipVxNotKK(op)
		break
	//Skip next instruction if Vx = Vy.
	case 0x5000:
		c.InsSkipVxVy(op)
		break
	//Const Sets VX to KK.
	case 0x6000:
		c.InsSetVXKK(op)
		break
	case 0x7000:
		c.InsAddKKtoVX(op)
		break
	case 0x8000:
		x := GetAddressX(op)
		y := GetAddressY(op)
		switch op & 0x000F {
		// 8xy0 - LD Vx, Vy
		case 0x0000:
			c.InsLDVxVy(x, y)
			break
		// 8xy1 - OR Vx, Vy
		case 0x0001:
			// Set Vx = Vx OR Vy.
			c.InsLDORVxVy(x, y)
			break
		// 8xy2 - AND Vx, Vy
		case 0x0002:
			// Set Vx = Vx AND Vy.
			c.InsLDANDVxVy(x, y)
			break
		// 8xy3 - XOR Vx, Vy
		case 0x0003:
			c.InsLDXORVxVy(x, y)
			break
		// 8xy4 - ADD Vx, Vy
		case 0x0004:
			c.InsADDVxVy(x, y)
			break
		// 8xy5 - SUB Vx, Vy
		case 0x0005:
			c.InsSUBVxVy(x, y)
			break
		// 8xy6 - SHR Vx {, Vy}
		case 0x0006:
			c.InsSHRVx(x)
			break
		// 8xy7 - SUBN Vx, Vy
		case 0x0007:
			c.InsSUBN(x, y)
			break
		// 8xyE - SHL Vx {, Vy}
		case 0x000E:
			c.InsSHLVx(x)
			break
		}
		break
	case 0x9000:
		x := GetAddressX(op)
		y := GetAddressY(op)
		switch op & 0x000F {
		// 9xy0 - SNE Vx, Vy
		case 0x0000:
			// Skip next instruction if Vx != Vy.
			// The values of Vx and Vy are compared, and if they are
			// not equal, the program counter is increased by 2.
			if c.v[x] != c.v[y] {
				c.pc += 2
			}
			c.pc += 2
			break
		}
	case 0xA000:
		//I = NNN
		c.i = GetAddressNNN(op)
		c.pc += 2
		break
	case 0xB000:
		//BNNN 	Flow 	PC=V0+NNN 	Jumps to the address NNN plus V0.
		//c.pc = uint16(c.v[0]) + (op & 0x0FFF)
		nnn := GetAddressNNN(op)
		pc := uint16(c.v[0]) + nnn
		c.pc = pc
		break
	case 0xC000:
		//CXNN 	Rand 	Vx=rand()&NN
		//Sets VX to the result of a bitwise and operation on a random number (Typically: 0 to 255) and NN.
		x := GetAddressX(op)
		kk := byte(GetAddressKK(op))
		vx := byte(rand.Intn(0xFF)) & kk
		c.v[x] = vx
		c.pc += 2
		break
	case 0xD000:
		//DXYN 	Disp 	draw(Vx,Vy,N)
		c.drawSprite(GetAddressX(op), GetAddressY(op), GetAddressN(op))
		c.draw = true
		c.pc += 2
		break
	case 0xE000:
		x := GetAddressX(op)
		kk := GetAddressKK(op)
		key := c.v[x]
		switch kk {
		case 0x9E:
			// EX9E 	KeyOp 	if(key()==Vx) 	Skips the next instruction if the key stored in VX is pressed.
			// (Usually the next instruction is a jump to skip a code block)
			if c.key[key] != 0 {
				c.pc += 2
			}
			c.pc += 2
			break
		case 0xA1:
			//EXA1 	KeyOp 	if(key()!=Vx) 	Skips the next instruction if the key stored in VX isn't pressed.
			// (Usually the next instruction is a jump to skip a code block)
			if c.key[key] == 0 {
				c.pc += 2
			}
			c.pc += 2
			break
		}
		break
	case 0xF000:
		x := GetAddressX(op)
		kk := GetAddressKK(op)
		switch kk {
		case 0x07:
			//FX07 	Timer 	Vx = get_delay() 	Sets VX to the value of the delay timer.
			c.v[x] = c.delay
			c.pc += 2
			break
		case 0x0A:
			//FX0A 	KeyOp 	Vx = get_key()
			//A key press is awaited, and then stored in VX.
			//(Blocking Operation. All instruction halted until next key event)
			for i := range c.key {
				if c.key[i] > 0 {
					c.v[x] = byte(i)
					c.pc += 2
					break
				}
			}
			break
		case 0x15:
			//FX15 	Timer 	delay_timer(Vx) 	Sets the delay timer to VX.
			c.delay = c.v[x]
			c.pc += 2
			break
		case 0x18:
			//FX18 Sets the sound timer to VX.
			c.sound = int(c.v[x])
			c.pc += 2
			break
		case 0x1E:
			//FX1E I +=Vx Adds VX to I.
			c.i = c.i + uint16(c.v[x])
			c.pc += 2
			break
		case 0x29:
			//FX29 I=sprite_addr[Vx]
			//Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font.
			c.i = uint16(c.v[x]) * 0x05
			c.pc += 2
			break
		case 0x33:
			//FX33 	BCD 	set_BCD(Vx);
			/*
				*(I+0)=BCD(3);
				*(I+1)=BCD(2);
				*(I+2)=BCD(1);
				Stores the binary-coded decimal representation of VX,
				with the most significant of three digits at the address in I,
				the middle digit at I plus 1, and the least significant digit at I plus 2.
				(In other words, take the decimal representation of VX, place the hundreds digit
				in memory at location in I, the tens digit at location I+1, and the ones digit
				at location I+2.)
			*/
			c.memory[c.i] = c.v[x] / 100
			c.memory[c.i+1] = (c.v[x] / 10) % 10
			c.memory[c.i+2] = (c.v[x] % 100) % 10
			c.pc += 2
			break
		case 0x55:
			//FX55 	MEM 	reg_dump(Vx,&I) 	Stores V0 to VX (including VX) in memory starting at address I.
			// The offset from I is increased by 1 for each value written, but I itself is left unmodified.
			var i uint16
			for i = 0; i <= x; i++ {
				c.memory[c.i+i] = c.v[i]
			}
			c.pc += 2
			break
		case 0x65:
			//FX65 	MEM 	reg_load(Vx,&I)
			//Fills V0 to VX (including VX) with values from memory starting at address I.
			//The offset from I is increased by 1 for each value written, but I itself is left unmodified.
			var i uint16
			for i = 0; uint16(i) <= x; i++ {
				c.v[i] = c.memory[c.i+i]
			}
			c.pc += 2
			break
		}
		break
	default:
		fmt.Printf("unknown: %#X %[1]v\n", op)
	}
}

func (c *chip8) drawSprite(x uint16, y uint16, n uint16) {
	/*DXYN 	Disp 	draw(Vx,Vy,N)
	Draws a sprite at coordinate (VX, VY)
		that has a width of 8 pixels and a height of N pixels.
		Each row of 8 pixels is read as bit-coded starting from memory location I;
		I value doesn’t change after the execution of this instruction.
		As described above, VF is set to 1 if any screen pixels are flipped from set to
		unset when the sprite is drawn, and to 0 if that doesn’t happen
		D-A-B-6
	*/
	//	x+ := (op & 0x0F00) >> 8
	//	y := (op & 0x00F0) >> 4
	//	n := (op & 0x000F)
	gfx := c.graphics
	x = uint16(c.v[x])
	y = uint16(c.v[y])

	var xline, yline uint16
	for yline = 0; yline < n; yline++ {
		pixel := c.memory[c.i+yline]
		for xline = 0; xline < 8; xline++ {
			if pixel&(0x80>>xline) != 0 {
				l := (x + xline) + ((y + yline) * 64)
				abs := l % 2048
				if gfx[abs] == 0x01 {
					c.v[0xF] = 1
				} else {
					c.v[0xF] = 0
				}
				gfx[abs] ^= 1
			}
		}
	}
}

//Bitwise to get opcode from 1st/2nd bytes.
func _getOpCode(b uint16, b2 uint16) uint16 {
	/*
		memory[pc]     == 0xA2
		memory[pc + 1] == 0xF0
		1010001000000000 |  // 0xA200
				11110000 =  // 0xF0 (0x00F0)
		------------------
		1010001011110000    // 0xA2F0
	*/
	return (b << 8) | b2
}

//ExtractCode Extract Opcodes
func _extractCode(op uint16) uint16 {
	/*
		Execute opcode
		1010001011110000 &  // 0xA2F0 (op)
		0000111111111111 =  // 0x0FFF
		------------------
		0000001011110000    // 0x02F0 (0x2F0)
	*/
	return op & 0x0FFF
}

func CreateSysFont() []byte {
	return []byte{
		0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
		0x20, 0x60, 0x20, 0x20, 0x70, // 1
		0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
		0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
		0x90, 0x90, 0xF0, 0x10, 0x10, // 4
		0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
		0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
		0xF0, 0x10, 0x20, 0x40, 0x40, // 7
		0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
		0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
		0xF0, 0x90, 0xF0, 0x90, 0x90, // A
		0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
		0xF0, 0x80, 0x80, 0x80, 0xF0, // C
		0xE0, 0x90, 0x90, 0x90, 0xE0, // D
		0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
		0xF0, 0x80, 0xF0, 0x80, 0x80, // F
	}
}
